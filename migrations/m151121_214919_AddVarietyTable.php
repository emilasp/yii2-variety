<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151121_214919_AddVarietyTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    public function up()
    {
        $this->createTable('varieties_variety', [
            'id'         => $this->primaryKey(11),
            'code'       => $this->string(50)->notNull(),
            'name'       => $this->string(50)->notNull(),
            'value'      => $this->string(14),
            'group'      => $this->string(150)->notNull(),
            'role'       => $this->string(14),
            'status'     => $this->smallInteger(1)->notNull(),
            'order'       => $this->smallInteger(2)->defaultValue(1),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_varieties_variety_created_by',
            'varieties_variety',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_varieties_variety_updated_by',
            'varieties_variety',
            'updated_by',
            'users_user',
            'id'
        );
        $this->createIndex('varieties_variety_code', 'varieties_variety', 'code');

        $this->createIndex('varieties_variety_status', 'varieties_variety', 'status');

        $this->addStatusesVariety();
        $this->addBoolVariety();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('varieties_variety');

        $this->afterMigrate();
    }

    private function addStatusesVariety()
    {
        Variety::add('status', 'status_enabled', 'Включено', 1, 1);
        Variety::add('status', 'status_disabled', 'Выключено', 0, 0);
    }


    private function addBoolVariety()
    {
        Variety::add('bool', 'bool_yes', 'Да', 1, 1);
        Variety::add('bool', 'bool_no', 'Нет', 0, 0);
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        $this->db->schema->refresh();
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
