<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\variety\models\Variety */

$this->title = Yii::t('variety', 'Create Variety');
$this->params['breadcrumbs'][] = ['label' => Yii::t('variety', 'Varieties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="variety-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
