<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\variety\models\Variety */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('variety', 'Variety'),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('variety', 'Varieties'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="variety-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
