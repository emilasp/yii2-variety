<?php
namespace emilasp\variety\models;

use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\caching\DbDependency;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use emilasp\user\core\models\User;
use emilasp\core\components\base\ActiveRecord;

/**
 * This is the model class for table "varieties_variety".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $value
 * @property string $group
 * @property string $role
 * @property integer $status
 * @property integer $order
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Variety extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'varieties_variety';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'group', 'status'], 'required'],
            [['status', 'order', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['code', 'name'], 'string', 'max' => 50],
            [['value', 'role'], 'string', 'max' => 14],
            [['group'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'code'       => Yii::t('site', 'Code'),
            'name'       => Yii::t('site', 'Name'),
            'value'      => Yii::t('site', 'Value'),
            'group'      => Yii::t('site', 'Group'),
            'role'       => Yii::t('site', 'Role'),
            'status'     => Yii::t('site', 'Status'),
            'order'      => Yii::t('site', 'Order'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'created_by' => Yii::t('site', 'Created By'),
            'updated_by' => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /** Поулчаем группы справочников
     * @return array
     */
    public static function getGroups()
    {
        return Yii::$app->db->cache(
            function () {
                return ArrayHelper::map(Variety::find()->select('group')->distinct()->all(), 'group', 'group');
            },
            null,
            new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM ' . self::tableName()])
        );
    }

    /** Поулчаем роли справочников
     * @return array
     */
    public static function getRoles()
    {
        return Yii::$app->db->cache(
            function () {
                return ArrayHelper::map(Variety::find()->select('role')->distinct()->all(), 'role', 'role');
            },
            null,
            new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM ' . self::tableName()])
        );
    }

    /** Поулчаем значения справочников
     * @return array
     */
    public static function getVariety($group, $fileld = 'name', $fieldSecond = 'value')
    {
        $all = Yii::$app->db->cache(
            function () use ($group, $fileld, $fieldSecond) {
                return ArrayHelper::map(
                    Variety::find()->where(['group' => $group])->orderBy('order DESC')->all(),
                    $fileld,
                    $fieldSecond
                );
            },
            null,
            new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM ' . self::tableName()])
        );
        return $all;
    }

    /** Получаем значение из справочника
     *
     * @param $code
     *
     * @return mixed
     */
    public static function getValue($code)
    {
        $variety = Yii::$app->db->cache(
            function () use ($code) {
                return Variety::find()->select('value')->where(['code' => $code])->one();
            },
            null,
            new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM ' . self::tableName()])
        );
        return ($variety) ? (int)$variety->value : null;
    }

    /** Получаем набор значений из справочника
     *
     * @param $group
     * @param $asArray
     *
     * @return mixed
     */
    public static function getValues($group, $asArray = true)
    {
        $varietyes = Yii::$app->db->cache(
            function () use ($group) {
                return Variety::find()->where(['group' => $group])->all();
            },
            null,
            new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM ' . self::tableName()])
        );

        if ($asArray) {
            $varietyes = ArrayHelper::map($varietyes, 'value', 'name');
        }
        return $varietyes;
    }

    /** Сохраняем элемент в справочник
     *
     * @param $name
     * @param $code
     * @param $group
     * @param $value
     * @param $order
     * @param null $role
     */
    public static function add($group, $code, $name, $value, $order, $role = '')
    {
        $status = self::STATUS_ENABLED;
        $sql    = <<<SQL
        INSERT INTO varieties_variety ("name", "code", "group", "value", "order", "role", "status",
         created_at, updated_at, created_by, updated_by)
        VALUES ('{$name}', '{$code}', '{$group}', '{$value}', {$order}, '{$role}', {$status},
        NOW(), NOW(), 1, 1);
SQL;
        Yii::$app->db->createCommand($sql)->execute();
    }
}
