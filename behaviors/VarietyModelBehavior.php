<?php
namespace emilasp\variety\behaviors;

use yii;
use yii\helpers\Inflector;
use yii\helpers\ArrayHelper;
use yii\caching\DbDependency;
use emilasp\variety\models\Variety;
use emilasp\core\behaviors\base\DynamicAttributeBehavior;

/** Добавляем поведение в модель
 * public function behaviors()
 * {
 *     return ArrayHelper::merge([
 *         'variety_type' => [
 *             'class'     => VarietyModelBehavior::className(),
 *             'attribute'  => 'type',
 *             'group' => 'product_type',
 *         ],
 *     ], parent::behaviors());
 * }
 *
 * Class VarietyModelBehavior
 * @package emilasp\variety\behaviors
 */
class VarietyModelBehavior extends DynamicAttributeBehavior
{
    /** @var string имя атрибута */
    public $attribute;
    /** @var string группа */
    public $group;
    /** @var  integer время кеширования */
    public $cacheDuration;

    public function init()
    {
        if (!$this->cacheDuration) {
            $this->cacheDuration = Yii::$app->getModule('variety')->getSetting('variety_cache_duration');
        }
        $this->attribute = Inflector::pluralize($this->attribute);
        parent::init();
    }

    /** Получаем вариации для справочника
     * @return mixed
     */
    protected function getAttribute($name)
    {
        $dependency = new DbDependency([
            'sql' => 'SELECT MAX(updated_at) FROM ' . Variety::tableName()
        ]);

        $status = Variety::STATUS_ENABLED;

        $sql = <<<SQL
            SELECT name, "value" FROM varieties_variety 
            WHERE "group"='{$this->group}' AND status='{$status}'
            ORDER BY "order" ASC 
SQL;

        return Yii::$app->db->cache(
            function () use ($sql) {
                return ArrayHelper::map(
                    Yii::$app->db->createCommand($sql)->queryAll(),
                    'value',
                    'name'
                );
            },
            $this->cacheDuration
            /*$dependency*/
        );
    }

    /** не используется, но объявлено в абстрактном классе
     * @param string $value
     */
    protected function setAttribute($name, $value) {

    }
}
