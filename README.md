Модуль Variety для Yii2
=============================

Модуль со справочниками

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-variety": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-variety.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'variety' => []
    ],
```
